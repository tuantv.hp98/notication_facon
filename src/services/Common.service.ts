import ApiCommon from "../../src/common/ApiCommon.common";

class CommonService {
    public async pushNoticationFacon(message: string): Promise<void> {
        return await ApiCommon.postFaconNotication(message);
    }
}
export default new CommonService();
