import axios from "axios"
import OtherCommon from "./OtherCommon.common";
class ApiCommon {
    public async postFaconNotication(message: string): Promise<void> {
        const generateId = OtherCommon.generateId(16);
        const messageParams = {
            msg: 'method',
            method: 'sendMessage',
            params: [
                {
                    _id: generateId,
                    rid: process.env.FACON_GROUP_ID || '3dpzyxqByPjyRmwYn',
                    msg: message,
                }
            ]
        }
        const params = {
            message: JSON.stringify(messageParams),
        }
        await axios({
            method: 'POST',
            url: process.env.FACON_URL + '/sendMessage',
            headers: {
                'X-Auth-Token': process.env.FACON_TOKEN,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest', 
                'X-User-Id': process.env.FACON_USER_ID,
            },
            data: params,
        }).then((res) => {
            console.log('Push messages to FACON success: ', res.status, res.data);
        }).catch((err) => {
            console.log('Push messages to FACON success: ' + err.stack);
        });
    }

    public async getChannelIdByName(group_name: string) {
        const messageParams = {
            msg: 'method',
            method: 'getRoomByTypeAndName',
            params: ['p', group_name],
        }
        const params = {
            message: JSON.stringify(messageParams),
        }
        const results = await axios({
            method: 'POST',
            url: process.env.FACON_URL + '/getRoomByTypeAndName',
            headers: {
                'X-Auth-Token': process.env.FACON_TOKEN,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest', 
                'X-User-Id': process.env.FACON_USER_ID,
            },
            data: params,
        })
        const channelDetail = JSON.parse(results.data.message);
        if (channelDetail.error) {
            throw new Error('Channel not exists');
        } else {
            return {
                channel_id: channelDetail.result._id
            }
        }
        
    }
}

export default new ApiCommon();
