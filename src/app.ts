import express, { Request, Response } from 'express';
import * as bodyParser from 'body-parser';
import { IRouteDefinition } from './common/definitions/Route.definition';
import CommonController from './controllers/Common.controller';

class App {
    public app: express.Application;
    constructor() {
        this.app = express();
        this.config();
        this.initRouter();
    }
    private config(): void {
        this.app.use(bodyParser.json({limit: '200mb'}));
        this.app.use(bodyParser.urlencoded({limit: '200mb',extended: false}));
    }

    private initRouter(): void {
        const listController: any = [
            CommonController,
        ];
        listController.forEach((controller: any) => {
            const instance = new controller();
            const prefix = Reflect.getMetadata('prefix', controller);
            const routes: IRouteDefinition[] = Reflect.getMetadata('routes', controller);
            routes.forEach((route) => {
                this.app[route.requestMethod](prefix + route.path, async (req: Request, res: Response) => {
                  try {
                    const data: any = await instance[route.methodName](req, res);
                    res.status(200);
                    res.json(data);
                  } catch (e) {
                      res.status(400);
                      res.json({
                          error: 1,
                          msg: e.message
                      })
                  }
                });
              });
        });
    }
}

export default new App().app;
