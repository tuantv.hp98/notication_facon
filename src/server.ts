import 'reflect-metadata';
import app from './app';
import dotenv from 'dotenv';

dotenv.config();

const PORT = process.env.PORT || 5000;

(async () => {
    app.listen(PORT, () => {
        console.log('Server running with port: ', PORT)
    });
})();
