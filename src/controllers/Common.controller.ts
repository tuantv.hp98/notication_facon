import { Post } from "../../src/common/decorator/Post.decorator";
import { Controller } from "../../src/common/decorator/Controller.decorator";
import CommonService from "../../src/services/Common.service";

@Controller('/common')
export default class CommonController {
    @Post('/push-facon-message')
    public async pushMessageFacon(req: any): Promise<any> {
        await CommonService.pushNoticationFacon(req.body.message || '');
        return {
            message: "success"
        }
    }
}
